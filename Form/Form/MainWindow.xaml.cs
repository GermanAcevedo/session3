﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Form
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Form loaded");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = txtText.Text;
            if (name.Length == 0)
            {
                MessageBox.Show("Please enter your name","Error",MessageBoxButton.OK,MessageBoxImage.Error);
                return;
            }
            string age = "";
            if (rbtnBelow20.IsChecked == true)
            {
                age = rbtnBelow20.Content.ToString();
            }
            else if (rbtnOver20.IsChecked == true)
            {
                age = rbtnOver20.Content.ToString();
            }
            else if (rbtnOver80.IsChecked == true)
            {
                age = rbtnOver80.Content.ToString();
            }
            else
            {
                MessageBox.Show("Choose one of the age options");
                return;
            }

            List<string> petList = new List<string>();

            if (chboxCat.IsChecked == true)
            {
                petList.Add(chboxCat.Content.ToString());
            }

            if (chboxDog.IsChecked == true)
            {
                petList.Add(chboxDog.Content.ToString());
            }

            if (chboxOther.IsChecked == true)
            {
                petList.Add(chboxOther.Content.ToString());
            }
            
            

            string continent = cmbContinent.Text;

            string Preferredtemp = lblTemp.Content.ToString();

            string pets = string.Join(", ", petList);

            File.WriteAllText("@../../info.txt", $"{name};{age};{pets};{continent};{Preferredtemp}");

            MessageBox.Show("The form is loaded", "Form loaded", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation );
        }
    }
}
